# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader


def index(request):
    template = loader.get_template('t18.html')
    context = {}
    return HttpResponse(template.render(context, request))

def mix(request):
    template = loader.get_template('mix.html')
    context = {}
    return HttpResponse(template.render(context, request))

def newindex(request):
    template = loader.get_template('t19.html')
    context = {}
    return HttpResponse(template.render(context, request))

def about(request):
    template = loader.get_template('about.html')
    context = {}
    return HttpResponse(template.render(context, request))

def video(request):
    template = loader.get_template('video.html')
    context = {}
    return HttpResponse(template.render(context, request))
