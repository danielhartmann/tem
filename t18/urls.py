from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^mix/$', views.mix),
    url(r'^t19/$', views.newindex, name='newindex'),
    url(r'^t19/sobre-a-tem.txt', views.about),
    url(r'^t19/clipe.mp4', views.video),
]
