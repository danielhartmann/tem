$( document ).ready(function() {
    var drums = new Howl({src: ["/static/music/loop drums.mp3"], preload: true});
    var bass = new Howl({src: ["/static/music/loop bass.mp3"], preload: true});
    var keys = new Howl({src: ["/static/music/loop keys.mp3"], preload: true});
    var guitar1 = new Howl({src: ["/static/music/loop guitar1.mp3"], preload: true});
    var guitar2 = new Howl({src: ["/static/music/loop guitar2.mp3"], preload: true});
    var guitar3 = new Howl({src: ["/static/music/loop guitar3.mp3"], preload: true});

    var instruments = [drums, bass, keys, guitar1, guitar2, guitar3];

    function play_instrument(instrument) {
      for(var i=0; i<instruments.length; i++) {
        instruments[i].stop();
        if (instruments[i]._src.includes(instrument)) {
          instruments[i].play();
        }
      }
    }

    function play_all() {
      for(var i=0; i<instruments.length; i++) {
        instruments[i].play();
      }
    }

    var playing_instruments = [];
    
    function stop_song() {
      for(var i=0; i<instruments.length; i++) {
        instruments[i].stop();
      }
      for(var i=0; i<playing_instruments.length; i++) {
        playing_instruments[i].stop();
      }
    }

    function play_song() {
      $('#playsong').css('display', 'none');
      $('#stopsong').css('display', 'block');
      stop_song();

      var drums_count = 0;
      var bass_count = 0;
      var keys_count = 0;
      var guitar1_count = 0;
      var guitar2_count = 0;
      var guitar3_count = 0;
      $('#track1 > *,#track2 > *,#track3 > *,#track4 > *,#track5 > *,#track6 > *').each(function() {
        instrument_name = $(this).attr('data-name');
        if (instrument_name == 'drums') { drums_count++; }
        if (instrument_name == 'bass') { bass_count++; }
        if (instrument_name == 'keys') { keys_count++; }
        if (instrument_name == 'guitar1') { guitar1_count++; }
        if (instrument_name == 'guitar2') { guitar2_count++; }
        if (instrument_name == 'guitar3') { guitar3_count++; }
      });
      
      function play_instrument_loop(instrument, counter) {
        console.log(instrument._src);
        console.log(counter);
        var sound = new Howl({
          src: [instrument._src],
          preload: true,
          onend: function () {
            counter--;
            if (counter > 0) {
              play_instrument_loop(instrument, counter);
            }
          }
        })
        sound.play();
        playing_instruments.push(sound);
      }

      if (drums_count > 0) { play_instrument_loop(drums, drums_count); }
      if (bass_count > 0) { play_instrument_loop(bass, bass_count); }
      if (keys_count > 0) { play_instrument_loop(keys, keys_count); }
      if (guitar1_count > 0) { play_instrument_loop(guitar1, guitar1_count); }
      if (guitar2_count > 0) { play_instrument_loop(guitar2, guitar2_count); }
      if (guitar3_count > 0) { play_instrument_loop(guitar3, guitar3_count); }
    }

    $('#playsong').on('click', function() {
      play_song();
    });
    
    $('#stopsong').on('click', function() {
      $('#playsong').css('display', 'block');
      $('#stopsong').css('display', 'none');
      stop_song();
    });

    $('.preview').on('click', function() {
      play_instrument($(this).prop('id'));
    });
    
    // DRAG AND DROP STUFF
    $('#instruments,#track1,#track2,#track3,#track4,#track5,#track6').sortable({
    	// SortableJS options go here
    	// See: (https://github.com/SortableJS/Sortable#options)

    	handle: '.connectedSortable',
    	invertSwap: true,
      group: {
              name: 'shared',
              pull: 'clone' // To clone: set pull to 'clone'
          },
          animation: 150,
    	// . . .
    });
    // $( function() {
//       $( "#instruments").sortable({
//         connectWith: ".connectedSortable",
//         forcePlaceholderSize: false,
//         helper: function (e, li) {
//             copyHelper = li.clone().insertAfter(li);
//             return li.clone();
//         },
//         stop: function () {
//             copyHelper && copyHelper.remove();
//         }
//       }).disableSelection();
//         $( "#track1,#track2,#track3,#track4,#track5,#track6").sortable({
//           connectWith: ".connectedSortable",
//         }).disableSelection();
//         $(".connectedSortable").sortable({
//             receive: function (e, ui) {
//                 copyHelper = null;
//             }
//         });
//     });
});