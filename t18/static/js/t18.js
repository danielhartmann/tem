( function( $ ) {
  'use strict';
  
  // Assumptions:
  // 1. The post content (and the video) will be inside a wrapper with class .post-content
  // 2. The video aspect ratio is 16:9
  // 3. The video will take the full width of the .post-content but will not go beyond a certain width
  
  $( window ).on( 'load resize', function() {
    
    // Take the width of the wrapper
    var contentWidth = $( '.video' ).width();
    
    // Define te maximum width of the video
    var videoMaxWidth = 824; // Change this pixel value to something that works for you
    
    // Resize all YouTube iframes according to the wrapper size with 16:9 aspect ratio
    $( '.video iframe[src*="https://www.youtube.com"]' ).each( function() {
      if ( $( this ).width() < contentWidth ) {
        $( this ).width( videoMaxWidth );
        $( this ).height( videoMaxWidth * 9 / 16 );
      } else {
        $( this ).width( contentWidth );
        $( this ).height( contentWidth * 9 / 16 );
      }
    } );
    
     $( '.video .cover').click(function () {
         this.nextElementSibling.style.display='block';
         this.style.display='none';
     });

    // // Take the width of the wrapper
    // var contentWidth = $( '.spotify' ).width();
    
    // // Define te maximum width of the video
    // var spotifyMaxWidth = 400; // Change this pixel value to something that works for you
    
    // // Resize all YouTube iframes according to the wrapper size with 16:9 aspect ratio
    // var iframe = $('.spotify iframe');
    // if ( iframe.width() < contentWidth ) {
    //   iframe.width( spotifyMaxWidth );
    // } else {
    //   iframe.width( contentWidth );
    // }

  } );
  
} ) ( jQuery );